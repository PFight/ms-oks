unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    SpinEdit1: TSpinEdit;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses UModel;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  Model : TModel;
  i : integer;
  stay : integer;

begin
  Memo1.Clear;
  for i := 1 to 5 do
    begin
      Model := TModel.Create(i, SpinEdit1.Value); //�������� ������������� ������
      Model.Run; //������ �������������
      stay := Model.GetStay; //��������� ������� �������

      //����� ������
      Memo1.Lines.Add('Workers: '+IntToStr(i));
      Memo1.Lines.Add('Stay cost: '+IntToStr(stay*50));
      Memo1.Lines.Add('Work cost: '+IntToStr(MaxTime*i*10 div 60));
      Memo1.Lines.Add('Hour cost: '+IntToStr(60*Round((MaxTime*i*10+stay*50)/MaxTime)));
      Model.OutData(Form1.Memo1);
      Memo1.Lines.Add('');
      Model.Free;
    end;
end;

end.
