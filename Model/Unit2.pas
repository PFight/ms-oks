unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Samples.Spin, UModel;

type
  TForm2 = class(TForm)
    Button1: TButton;
    SpinEdit1: TSpinEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    PCount: TSpinEdit;
    Button2: TButton;
    ButtonVelch: TButton;
    ButtonHar: TButton;
    ButtonExperiment2k: TButton;
    procedure Button1Click(Sender: TObject);
    procedure ButtonVelchClick(Sender: TObject);
    procedure SetupParameters(model: SimulationModel);
    procedure Button2Click(Sender: TObject);
    procedure ButtonHarClick(Sender: TObject);
    procedure ButtonExperiment2kClick(Sender: TObject);
    function MakeExperiment(k: real; T4, T5: integer): real; // Returns profit
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

// main experiment
procedure TForm2.Button1Click(Sender: TObject);
var model: SimulationModel;
   T4min, T4max, T5min, T5max: integer;
   profit: real;
   mExperimentResultFile : TextFile;
   T4, T5, i : integer;
begin
    model:= SimulationModel.Create;
    SetupParameters(model);
    T4min := model.PRM_MIN_POSSIBLY_T4_AND_T5;
    T4max := model.PRM_T4_med;
    T5min := model.PRM_MIN_POSSIBLY_T4_AND_T5;
    T5max := model.PRM_T5_med;

    //DecimalSeparator:= ',';

    AssignFile(mExperimentResultFile, 'Experiment.txt');
    ReWrite(mExperimentResultFile);
    Writeln(mExperimentResultFile,  '���������� ������������');
    Writeln(mExperimentResultFile);
    Writeln(mExperimentResultFile,  'T4 | T5 | �������');

    Caption := '���������� ������������: ' + FloatToStr(0.0) + '%';

    i:=0;
    for T5 := T5min to T5max do begin
        for T4 := T4min to T4max do begin
            profit := MakeExperiment(model.PRM_k, T4, T5);
            Writeln(mExperimentResultFile,  T4, '   | ', T5, '  | ', profit:4:2);
            inc(i);
            Caption := '���������� ������������: ' + FloatToStrF(i/15*100, ffGeneral, 2, 4) + '%';
        end;
    end;

    CloseFile(mExperimentResultFile);

    MessageBox(self.Handle, '���������� ����������� � ����� Experiment.txt', '������!', 0);

    Caption := '';

end;

procedure TForm2.Button2Click(Sender:   TObject);
var model: SimulationModel;
begin
    model := SimulationModel.Create();
    SetupParameters(model);
    model.Run(SpinEdit1.Value * 60, true);
    model.Free();
    MessageBox(self.Handle, '��������� � ������ log.txt � trace.txt', '������!', 0);
end;

// Returns profit
function TForm2.MakeExperiment(k: real; T4, T5: integer): real;
var model: SimulationModel;
    modelingTime, progon: integer;
    profit: real;
    costs: real;
begin
    modelingTime:= SpinEdit1.Value * 60;
    profit := 0;
    costs := 0;

    for progon:=1 to PCount.Value do begin
        model := SimulationModel.Create();
        SetupParameters(model);
        costs := (model.PRM_k - k)*model.PRM_S2 +
            (model.PRM_T4_med - T4)*model.PRM_S3 +
            (model.PRM_T5_med - T5)*model.PRM_S3;
        model.PRM_k := k;
        model.PRM_T4_med := T4;
        model.PRM_T5_med := T5;
        model.Run(modelingTime, false);

        profit := profit + model.mProfit - costs * model.mProductCount;

        model.Free();
    end;

    MakeExperiment:= profit / PCount.Value;
end;

procedure TForm2.ButtonExperiment2kClick(Sender: TObject);
var model: SimulationModel;
   kMin, kMax: real;
   T4min, T4max, T5min, T5max: integer;
   profit1,profit2,profit3,profit4,profit5,profit6,profit7,profit8: real;
   mExperimentResultFile : TextFile;
   k: real;
   T4, T5 : integer;
begin

    model:= SimulationModel.Create;
    SetupParameters(model);

    kMin := 0;
    kMax :=model.PRM_k;
    T4min := model.PRM_MIN_POSSIBLY_T4_AND_T5;
    T4max := model.PRM_T4_med;
    T5min := model.PRM_MIN_POSSIBLY_T4_AND_T5;
    T5max := model.PRM_T5_med;

    AssignFile(mExperimentResultFile, 'Experiment2k.txt');
    ReWrite(mExperimentResultFile);
    Writeln(mExperimentResultFile,  '���������� ������������ 2^k');
    Writeln(mExperimentResultFile);
    Writeln(mExperimentResultFile,  'k    | T4 | T5 | �������');

    Caption := '���������� ������������: ' + FloatToStr(0.0) + '%';

    k:=kMax; T4:= T4Max; T5 := T5Max;
    profit1 := MakeExperiment(k, T4, T5);
    Writeln(mExperimentResultFile,  k:4:2, ' | ', T4, '   | ', T5, '  | ', profit1:4:2);

    Caption := '���������� ������������: ' + FloatToStr(1/8*100) + '%';

    k:=kMin; T4:= T4Max; T5 := T5Max;
    profit2 := MakeExperiment(k, T4, T5);
    Writeln(mExperimentResultFile,  k:4:2, ' | ', T4, '   | ', T5, '  | ', profit2:4:2);

    Caption := '���������� ������������: ' + FloatToStr(2/8*100) + '%';

    k:=kMax; T4:= T4Min; T5 := T5Max;
    profit3 := MakeExperiment(k, T4, T5);
    Writeln(mExperimentResultFile,  k:4:2, ' | ', T4, '   | ', T5, '  | ', profit3:4:2);

    Caption := '���������� ������������: ' + FloatToStr(3/8*100) + '%';


    k:=kMin; T4:= T4Min; T5 := T5Max;
    profit4 := MakeExperiment(k, T4, T5);
    Writeln(mExperimentResultFile,  k:4:2, ' | ', T4, '   | ', T5, '  | ', profit4:4:2);

    Caption := '���������� ������������: ' + FloatToStr(4/8*100) + '%';

    k:=kMax; T4:= T4Max; T5 := T5Min;
    profit5 := MakeExperiment(k, T4, T5);
    Writeln(mExperimentResultFile,  k:4:2, ' | ', T4, '   | ', T5, '  | ', profit5:4:2);

    Caption := '���������� ������������: ' + FloatToStr(5/8*100) + '%';

    k:=kMin; T4:= T4Max; T5 := T5Min;
    profit6 := MakeExperiment(k, T4, T5);
    Writeln(mExperimentResultFile,  k:4:2, ' | ', T4, '   | ', T5, '  | ', profit6:4:2);

    Caption := '���������� ������������: ' + FloatToStr(6/8*100) + '%';

    k:=kMax; T4:= T4Min; T5 := T5Min;
    profit7 := MakeExperiment(k, T4, T5);
    Writeln(mExperimentResultFile,  k:4:2, ' | ', T4, '   | ', T5, '  | ', profit7:4:2);

    Caption := '���������� ������������: ' + FloatToStr(7/8*100) + '%';

    k:=kMin; T4:= T4Min; T5 := T5Min;
    profit8 := MakeExperiment(k, T4, T5);
    Writeln(mExperimentResultFile,  k:4:2, ' | ', T4, '   | ', T5, '  | ', profit8:4:2);

    Caption := '���������� ������������: ' + FloatToStr(8/8 *100) + '%';

    Writeln(mExperimentResultFile,  '������� ������ ������� k: ',
        (((profit2 - profit1) +
        (profit4 - profit3) +
        (profit6 - profit5) +
        (profit8 - profit7))/4) :4:2);
    Writeln(mExperimentResultFile,  '������� ������ ������� T4: ',
        (((profit3 - profit1) +
        (profit4 - profit2) +
        (profit7 - profit5) +
        (profit8 - profit6))/4) :4:2);
    Writeln(mExperimentResultFile,  '������� ������ ������� T5: ',
        (((profit5 - profit1) +
        (profit6 - profit2) +
        (profit7 - profit3) +
        (profit8 - profit4))/4) :4:2);



    CloseFile(mExperimentResultFile);

    MessageBox(self.Handle, '���������� ����������� � ����� Experiment2k.txt', '������!', 0);

    Caption := '';

end;

procedure TForm2.ButtonHarClick(Sender: TObject);
var model: SimulationModel;
    modelingTime, cuttedModelingTime, i, progon: integer;
    mHarFile : TextFile;
    sumProductivityPerSmena, sumProcessTime,
    sumT2Load, sumT3Load, sumT4Load, sumT5Load,
    sumProfitPerDay: real;
begin
    modelingTime:= SpinEdit1.Value * 60;

    sumProcessTime:=0;
    sumProductivityPerSmena:=0;
    sumT2Load:=0;
    sumT3Load:=0;
    sumT4Load:=0;
    sumT5Load:=0;
    sumProfitPerDay:=0;

    for progon:=1 to PCount.Value do begin
        model := SimulationModel.Create();
        SetupParameters(model);
        cuttedModelingTime:= cuttedModelingTime - model.mStatisticStartTime;
        model.Run(modelingTime, false);

        sumProductivityPerSmena := sumProductivityPerSmena +
            model.mProductCount / (cuttedModelingTime / (8*60)) *4;
        sumProcessTime := sumProcessTime +
            model.mSumProcessTime / model.mTransactNum / 4;
        sumT2Load := sumT2Load + model.mDeviceT2.TimeOfUsage / modelingTime;
        sumT3Load := sumT3Load + model.mDeviceT3.TimeOfUsage / modelingTime;
        sumT4Load := sumT4Load + model.mDeviceT4.TimeOfUsage / modelingTime;
        sumT5Load := sumT5Load + model.mDeviceT5.TimeOfUsage / modelingTime;

        sumProfitPerDay := sumProfitPerDay + model.mProfit / (cuttedModelingTime / (24*60)) *4;

        model.Free();
    end;

   AssignFile(mHarFile, 'haracteristics.txt');
   ReWrite(mHarFile);
   Writeln(mHarFile,  'C������ ������������������ ������� �� ����� (8 �����): ', sumProductivityPerSmena / PCount.Value:4:2);
   Writeln(mHarFile,  'C������ ����� ��������� �������: ', sumProcessTime / PCount.Value:4:2, ' min');
   Writeln(mHarFile,  '������� �������� ���������� �2: ', sumT2Load * 100 / PCount.Value:4:2, '%');
   Writeln(mHarFile,  '������� �������� ���������� �3: ', sumT3Load * 100 / PCount.Value:4:2, '%');
   Writeln(mHarFile,  '������� �������� ���������� �4: ', sumT4Load * 100 / PCount.Value:4:2, '%');
   Writeln(mHarFile,  '������� �������� ���������� �5: ', sumT5Load * 100 / PCount.Value:4:2, '%');
   Writeln(mHarFile,  '������� ���������� ������� (�� 24 ����) : ', sumProfitPerDay / PCount.Value:4:2);

   CloseFile(mHarFile);

   MessageBox(self.Handle, '�������������� ������� � ����� haracteristics.txt', '������!', 0);
end;

procedure TForm2.ButtonVelchClick(Sender: TObject);
var model: SimulationModel;
    mVelchDataSum : TArray<integer>;
    modelingTime, i, progon: integer;
    mVelchFile : TextFile;
    velchCount : integer;
begin
    modelingTime:= SpinEdit1.Value * 60;
    velchCount:= round(modelingTime / 10 + 0.5);
    SetLength(mVelchDataSum, velchCount+1);
    for i:=0 to velchCount do begin
      mVelchDataSum[i] := 0;
    end;

    for progon:=0 to PCount.Value do begin
        model := SimulationModel.Create();
        SetupParameters(model);
        model.Run(modelingTime, false);

        for i:=0 to velchCount do begin
          mVelchDataSum[i] := mVelchDataSum[i] + model.mVelchData[i];
        end;

        model.Destroy();
    end;

   AssignFile(mVelchFile, 'velch.txt');
   ReWrite(mVelchFile);
        for i:=0 to velchCount do begin
          Writeln(mVelchFile, i, ', ', mVelchDataSum[i]/ PCount.Value:4:2);
        end;
   CloseFile(mVelchFile);


    MessageBox(self.Handle, '���������� � ����� velch.txt', '������!', 0);
end;

procedure TForm2.SetupParameters(model: SimulationModel);
begin
  model.PRM_T1 := 7; // ������� ����� ����������� ������ (���������������� �������������)
  model.PRM_T2_med := 5; // ������� ���� ��������������� ��������� 1
  model.PRM_T2_spread := 2; // �������� �������� ������� ��������������� ��������� 1 (����������� �������������)
  model.PRM_T3_med := 5; // ������� ���� ��������������� ��������� 2
  model.PRM_T3_spread := 2; // �������� �������� ������� ��������������� ��������� 2 (����������� �������������)
  model.PRM_k := 0.1; // ������� ����� ����� ��������������� ���������
  model.PRM_T4_med := 5; // ������� ����� ������
  model.PRM_T4_spread := 2; // �������� �������� ������� ������ (����������� �������������)
  model.PRM_T5_med := 7; // ������� ����� �����������
  model.PRM_T5_spread := 1; // �������� �������� ������� ���������� (����������� �������������)
  model.PRM_S1 := 80; // ������� �� ������������ ������ �������
  model.PRM_NORMAL_TIME := 10; // ���� ������ �������, ������� ������ ��1, ���������� � ���� ����� 40 �����, ��������� ������� ����������� �����.
  model.PRM_S2 := 300; // ���������� ������ ����� �� �������� (k - r) ������� r*S2 ������ ��������� �� ������ ������
  model.PRM_S3 := 3; // ���������� ������� ����������������� �������� ������ � ����������� �� m ����� ������� ��������������� �������� m*S3 ������ ��������� �� ���� �������
  model.PRM_MIN_POSSIBLY_T4_AND_T5 := 3; // ���������� ��������� ������������ �������� ������ � �����������
  model.mStatisticStartTime := 200; // ����������� ��������� ���������� �������
end;

end.

