object Form2: TForm2
  Left = 0
  Top = 0
  ClientHeight = 181
  ClientWidth = 362
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 256
    Top = 20
    Width = 29
    Height = 13
    Caption = #1095#1072#1089#1086#1074
  end
  object Label4: TLabel
    Left = 16
    Top = 20
    Width = 112
    Height = 13
    Caption = #1042#1088#1077#1084#1103' '#1084#1086#1076#1077#1083#1080#1088#1086#1074#1072#1085#1080#1103
  end
  object Label7: TLabel
    Left = 16
    Top = 47
    Width = 120
    Height = 13
    Caption = #1050#1086#1083#1083#1080#1095#1077#1089#1090#1074#1086' '#1087#1088#1086#1075#1086#1085#1086#1074':'
  end
  object Button1: TButton
    Left = 194
    Top = 112
    Width = 152
    Height = 25
    Caption = #1054#1089#1085#1086#1074#1085#1086#1081' '#1101#1082#1089#1087#1077#1088#1080#1084#1077#1085#1090
    TabOrder = 0
    OnClick = Button1Click
  end
  object SpinEdit1: TSpinEdit
    Left = 153
    Top = 17
    Width = 97
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 800
  end
  object PCount: TSpinEdit
    Left = 153
    Top = 45
    Width = 97
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 2
    Value = 10
  end
  object Button2: TButton
    Left = 13
    Top = 78
    Width = 155
    Height = 25
    Caption = #1058#1077#1089#1090#1086#1074#1099#1081' '#1087#1088#1086#1075#1086#1085
    TabOrder = 3
    OnClick = Button2Click
  end
  object ButtonVelch: TButton
    Left = 13
    Top = 112
    Width = 155
    Height = 25
    Caption = #1055#1088#1086#1094#1077#1076#1091#1088#1072' '#1074#1077#1083#1095#1072
    TabOrder = 4
    OnClick = ButtonVelchClick
  end
  object ButtonHar: TButton
    Left = 13
    Top = 143
    Width = 155
    Height = 25
    Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1089#1080#1089#1090#1077#1084#1099
    TabOrder = 5
    OnClick = ButtonHarClick
  end
  object ButtonExperiment2k: TButton
    Left = 194
    Top = 78
    Width = 152
    Height = 25
    Caption = #1069#1082#1089#1087#1077#1088#1080#1084#1077#1085#1090' 2^k'
    TabOrder = 6
    OnClick = ButtonExperiment2kClick
  end
end
