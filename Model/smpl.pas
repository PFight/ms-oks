{$O-}
unit SMPL;

interface

uses
  Classes, SysUtils;

Type
  real = Extended;

  // forwarding �������
  TSMPLQueue = class;
  TSMPLDevice = class;
  TSMPLEvent = class;
  TSMPL = class;

  TSMPL = class
  private
    fTime: integer; // ������� ����� �������������
    EventList: TList; // ������ �������
    DeviceList: TList; // ������ ���������
    QueueList: TList; // ������ ��������

    fErrorOn: boolean; // ������������ ������
    fTraceOn: boolean; //������ Trace
    fTrace: TStringList;

    function getQueueListLength: integer;
    function getDeviceListLength: integer;
  public
    constructor Create; // �����������
    destructor Destroy; override; // ����������

    procedure AddQueue(Queue: TSMPLQueue); // �������� �������
    function CreateQueue(QueueName: String): TSMPLQueue; // ������� �������
    procedure AddDevice(Device: TSMPLDevice); // �������� ����������
    function CreateDevice(DeviceName: String): TSMPLDevice;
    // ������� ����������
    procedure AddEvent(Event: TSMPLEvent); // �������� �������
    function CreateEvent(EventID: integer; TransactID: integer;
      RelativeTime: integer): TSMPLEvent; // ������� �������

    function Poisson(M: integer): integer; // ������������� �������������
    function Exp(M: integer): integer; // ���������������� �������������
    function UniForm(minValue, maxValue: integer): integer;
    // ����������� �������������
    function Gauss(Mx, Sigma: integer): integer; // ���������� �������������

    function CauseEvent: TSMPLEvent; overload; // ������� ��������� �������
    procedure CauseEvent(var Event : TSMPLEvent); overload;
    function CancelEvent(EventID: integer; TransactID: integer): integer;
    // �������� ������� ��������������� ���������

    function MonitorEvents: TStringList; overload; // ������� �������
    function MonitorQueues: TStringList; overload; // ������� ��������
    function MonitorDevices: TStringList; overload; // ������� ���������
    function Monitor: TStringList; overload; // ����� �������

    procedure MonitorEvents(var sts : TStringList); overload;
    procedure MonitorQueues(var sts : TStringList); overload;
    procedure MonitorDevices(var sts : TStringList); overload;
    procedure Monitor(var sts : TStringList); overload;

    function ReportDevices: TStringList; overload; // ����� � ������ ��������
    function ReportQueues: TStringList; overload; // ����� � ������ ��������
    function Report: TStringList; overload; // ����� �����

    procedure ReportDevices(var sts : TStringList); overload;
    procedure ReportQueues(var sts : TStringList); overload;
    procedure Report(var sts : TStringList); overload;

    function GetQueueByID(QueueID: integer): TSMPLQueue;
    function GetDeviceByID(DeviceID: integer): TSMPLDevice;

    procedure ReInit;

    procedure AddTrace(Line: String);
    procedure Error(ErrorMessage: String);

    property Trace: TStringList read fTrace;
    property Time: integer read fTime { write fTime } ;
    // ������� ����� �������������
    property QueueCount: integer read getQueueListLength;
    // ���������� �������� � �������
    property DeviceCount: integer read getDeviceListLength;
    // ��������� ��������� � �������

    property TraceOn : Boolean read fTraceOn write fTraceOn; //�������� ����� �����������
    property ErrorOn : Boolean read fErrorOn write fErrorOn; //�������� ����� ���������� ������
  end;

  TSMPLDevice = class
  private
    fDeviceName: String;
    fLastTimeReserve: integer;
    fTransactID: integer;
    fCompleteUsageCount: integer;
    fTimeOfUsage: integer;
    fSMPL: TSMPL;
  public
    constructor Create(DeviceName: String; SMPL: TSMPL); // ������� ����������

    procedure Reserve(TransactID: integer); // ��������������� ����������
    procedure Release; // ���������� ����������

    property DeviceName: String read fDeviceName; // ��� ����������
    property LastTimeReserve: integer read fLastTimeReserve;
    // ����� ���������� ��������������
    property TransactID: integer read fTransactID; // ����� ���������
    property CompleteUsageCount: integer read fCompleteUsageCount;
    // ����� ���������� �����������
    property TimeOfUsage: integer read fTimeOfUsage;
    // ����� ����� �������������
    property Status: integer read fTransactID; // ������ ����������: 0-��������, ����� ����� ���������
  end;

  TSMPLQueueElement = class
  private
    fStage: integer;
    fPriority: integer;
    fTransactID: integer;
    fTime: integer;
  public
    constructor Create(TransactID: integer; Time: integer; Priority: integer;
      Stage: integer);

    property Stage: integer read fStage write fStage; // ������ ���������
    property Priority: integer read fPriority write fPriority; // ���������
    property TransactID: integer read fTransactID write fTransactID;
    // ����� ���������
    property Time: integer read fTime write fTime; // ����� ����������
  end;

  TSMPLQueue = class
  private
    fQueueName: String;
    fQueueList: TList;
    fMax: integer;
    fSTQ: real;
    fSW: real;
    fSW2: real;
    fLastUpdate: integer;
    fDeletedCount: integer;
    fSMPL: TSMPL; // ����� ��������
    function getSMPLQueueElement(Index: integer): TSMPLQueueElement;
    function getHead: TSMPLQueueElement;
    function getLength: integer;
  public
    constructor Create(QueueName: String; SMPL: TSMPL); // �������� �������
    destructor Destroy; override;

    procedure Add(const Value: TSMPLQueueElement); overload;
    // �������� ������� � ����� �������
    procedure Add(TransactID: integer; Priority: integer; Stage: integer);
      overload; // ������� � �������� ������� � ����� �������

    procedure Clear;

    property QueueName: String read fQueueName;
    property Head: TSMPLQueueElement read getHead;
    // ��������� ������� �� ������ ������
    property List[Index: integer]: TSMPLQueueElement read getSMPLQueueElement;
    default; // ������� �� �������
    property Count: integer read getLength; // ����� �������
    property Max: integer read fMax; // ������������ ����� �������
    property STQ: real read fSTQ; // ����� ������������ ����� ������ �� ����� � ������� �������� ��� ���� ����������
    property SW: real read fSW; // ����� ������� ��������
    property SW2: real read fSW2; // ����� ��������� ������� ��������
    property LastUpdate: integer read fLastUpdate;
    // ����� ���������� ��������� ����� �������
    property DeletedCount: integer read fDeletedCount;
    // ����� ��������� ���������� �� �������
  end;

  TSMPLEvent = class
    fTime: integer;
    fType: integer;
    fTransactID: integer;
  public
    constructor Create(EventType: integer; Time: integer; TransactID: integer);

    property Time: integer read fTime write fTime;  //�����, ����� ��������� �������
    property EventType: integer read fType; //��� �������
    property TransactID: integer read fTransactID; //����� ���������
  end;

implementation

function IntToStrLen(Num, len: integer): String;
begin
  Result := IntToStr(Num);
  len := len - Length(Result);
  while len > 0 do
  begin
    Result := ' ' + Result;
    dec(len);
  end;
end;

function StrToStrLen(St : String; len : integer) : String;
begin
  if Length(st) > len then
    delete(st, len + 1, Length(st)-len);
  Result := St;
  len := len - Length(Result);
  while len > 0 do
  begin
    Result := ' ' + Result;
    dec(len);
  end;
end;

function FloatToStrLen(Num : real; len: integer): String;
begin
  Result := FloatToStrF(Num, ffFixed, len, 2);
  len := len - Length(Result);
  while len > 0 do
  begin
    Result := ' ' + Result;
    dec(len);
  end;
end;

{ TSMPL }

procedure TSMPL.AddDevice(Device: TSMPLDevice);
begin
  if Device = Nil then
  begin
    Error('������: ���������� ���������� Nil');
    exit;
  end;

  AddTrace('���������� ���������� ' + Device.DeviceName);

  DeviceList.Add(Device);
end;

procedure TSMPL.AddEvent(Event: TSMPLEvent);
var
  i: integer;
  index: integer;

begin
  if Event = Nil then
  begin
    Error('������: ���������� ������� Nil');
    exit;
  end;

  if (Event.fTime < 0) then begin
    i :=0;
  end;


  AddTrace('[' + IntToStr(fTime) + '] Schedule on ' + IntToStr
      (Event.fTime + fTime) + ': Type: ' + IntToStr(Event.fType)
      + ' Transact ' + IntToStr(Event.fTransactID));

  Event.Time := Event.Time + fTime;

  index := 0;
  for i := EventList.Count - 1 downto 0 do
    if TSMPLEvent(EventList.Items[i]).fTime <= Event.Time then
    begin
      index := i + 1;
      break;
    end;

  EventList.Insert(index, Event);
end;

procedure TSMPL.AddQueue(Queue: TSMPLQueue);
begin
  if Queue = Nil then
  begin
    Error('������: ���������� ������� Nil');
    exit;
  end;

  AddTrace('���������� ������� ' + Queue.QueueName);

  QueueList.Add(Queue);
end;

procedure TSMPL.AddTrace(Line: String);
begin
  if fTraceOn then
    fTrace.Add(Line);
end;

function TSMPL.CancelEvent(EventID, TransactID: integer): integer;
var
  i: integer;
  fl: boolean;

begin
  AddTrace('[' + IntToStr(fTime) + '] Cancel event: Type ' + IntToStr(EventID)
      + ', Transact ' + IntToStr(TransactID));

  Result := 0;

  fl := true;
  for i := 0 to EventList.Count - 1 do
    if (TSMPLEvent(EventList.Items[i]).EventType = EventID) and
      (TSMPLEvent(EventList.Items[i]).TransactID = TransactID) then
    begin
      Result := TSMPLEvent(EventList.Items[i]).Time - fTime;
      EventList.Delete(i);
      fl := false;
      break;
    end;
  if fl then
  begin
    Error('������: �� ������� ������� �������: ��� ' + IntToStr(EventID)
        + ', �������� ' + IntToStr(TransactID));
    exit;
  end;
end;

procedure TSMPL.CauseEvent(var Event: TSMPLEvent);
begin
  if Event <> Nil then
    Event.Free;

  Event := CauseEvent;
end;

function TSMPL.CauseEvent: TSMPLEvent;
begin
  Result := Nil;

  if EventList.Count = 0 then
  begin
    Error('������� �������� �������, ����� ��� �������������� �������');
    exit;
  end;

  AddTrace('[' + IntToStr(fTime) + '] Cause event: Type ' + IntToStr
      (TSMPLEvent(EventList.Items[0]).EventType) + ', Transact ' + IntToStr
      (TSMPLEvent(EventList.Items[0]).TransactID));

  Result := TSMPLEvent(EventList.Items[0]);
  fTime := Result.Time;
  EventList.Delete(0);
end;

constructor TSMPL.Create;
begin
  RandomIZE;

  fTime := 0;
  EventList := TList.Create;
  DeviceList := TList.Create;
  QueueList := TList.Create;
  fTrace := TStringList.Create;

  fErrorOn := true;
  fTraceOn := true;

end;

function TSMPL.CreateDevice(DeviceName: String): TSMPLDevice;
var
  Device: TSMPLDevice;

begin
  Device := TSMPLDevice.Create(DeviceName, Self);
  Result := Device;
  AddDevice(Device);
end;

function TSMPL.CreateEvent(EventID, TransactID, RelativeTime: integer)
  : TSMPLEvent;
var
  Event: TSMPLEvent;

begin
  Event := TSMPLEvent.Create(EventID, RelativeTime, TransactID);
  Result := Event;
  AddEvent(Event);
end;

function TSMPL.CreateQueue(QueueName: String): TSMPLQueue;
var
  Queue: TSMPLQueue;

begin
  Queue := TSMPLQueue.Create(QueueName, Self);
  Result := Queue;
  AddQueue(Queue);
end;

destructor TSMPL.Destroy;
var
  i: integer;

begin
  for i := 0 to EventList.Count - 1 do
    TSMPLEvent(EventList.Items[i]).Free;
  EventList.Clear;
  EventList.Free;

  for i := 0 to DeviceList.Count - 1 do
    TSMPLDevice(DeviceList.Items[i]).Free;
  DeviceList.Clear;
  DeviceList.Free;

  for i := 0 to QueueList.Count - 1 do
    TSMPLQueue(QueueList.Items[i]).Free;
  QueueList.Clear;
  QueueList.Free;

  Trace.Clear;
  Trace.Free;

  inherited;
end;

procedure TSMPL.Error(ErrorMessage: String);
begin
  if fErrorOn then
  begin
    raise Exception.Create(ErrorMessage);
//    Self.Destroy;
  end;
end;

function TSMPL.Exp(M: integer): integer;
begin
  Result := Round(-M * ln(random));
end;

function TSMPL.Gauss(Mx, Sigma: integer): integer;
var
  x, y: Extended;
  z0, r: Extended;

begin
  // �������������� �����-�������
  repeat
    x := 2 * random - 1;
    y := 2 * random - 1;
    r := sqr(x) + sqr(y);
  until (r <= 1) and (r > 0);
  z0 := x * sqrt(-2 * ln(r) / r);

  Result := Round(Mx + Sigma * z0);
end;

function TSMPL.GetDeviceByID(DeviceID: integer): TSMPLDevice;
begin
  Result := Nil;

  if (DeviceID >= DeviceList.Count) or (DeviceID < 0) then
  begin
    Error('�������������� ���������� ' + IntToStr(DeviceID) + ' �� ����������');
    exit;
  end;

  Result := TSMPLDevice(DeviceList.Items[DeviceID]);
end;

function TSMPL.getDeviceListLength: integer;
begin
  Result := DeviceList.Count;
end;

function TSMPL.GetQueueByID(QueueID: integer): TSMPLQueue;
begin
  Result := Nil;

  if (QueueID >= QueueList.Count) or (QueueID < 0) then
  begin
    Error('������������� ������� ' + IntToStr(QueueID) + ' �� ����������');
    exit;
  end;

  Result := TSMPLQueue(QueueList.Items[QueueID]);
end;

function TSMPL.getQueueListLength: integer;
begin
  Result := QueueList.Count;
end;

function TSMPL.Monitor: TStringList;
var
  tmp : TStringList;

begin
  Result := TStringList.Create;

  Result.Add('����� �������������: '+IntToStr(fTime)+' ������.');
  Result.Add('');

  tmp := MonitorEvents;
  Result.AddStrings(tmp);
  Result.Add('');
  tmp.Free;

  tmp := MonitorDevices;
  Result.AddStrings(tmp);
  Result.Add('');
  tmp.Free;

  tmp := MonitorQueues;
  Result.AddStrings(tmp);
  tmp.Free;
end;

function TSMPL.MonitorDevices: TStringList;
var
  i : integer;

begin
  Result := TStringList.Create;
  Result.Add('         ������ ���������');
  Result.Add('| ��� ���������� | ����� ��������� |');
  Result.Add('+----------------+-----------------+');
  for i := 0 to DeviceList.Count - 1 do
    Result.Add('|'+StrToStrLen(TSMPLDevice(DeviceList.Items[i]).fDeviceName, 15)+' |'+IntToStrLen(TSMPLDevice(DeviceList.Items[i]).fTransactID, 16)+' |');
  Result.Add('+----------------+-----------------+');
end;

function TSMPL.MonitorEvents: TStringList;
var
  i: integer;

begin
  Result := TStringList.Create;
  Result.Add('                 ������ �������');
  Result.Add('+---------------+---------------+-----------------+');
  Result.Add('| ����� ������� | ����� ������� | ����� ��������� |');
  Result.Add('+---------------+---------------+-----------------+');
  for i := 0 to EventList.Count - 1 do
    Result.Add('|' + IntToStrLen(TSMPLEvent(EventList.Items[i]).Time, 14)
        + ' |' + IntToStrLen(TSMPLEvent(EventList.Items[i]).EventType, 14)
        + ' |' + IntToStrLen(TSMPLEvent(EventList.Items[i]).TransactID, 16)
        + ' |');
  Result.Add('+---------------+---------------+-----------------+');
end;

function TSMPL.MonitorQueues: TStringList;
var
  i, j: integer;

begin
  Result := TStringList.Create;
  Result.Add('              ������ ��������');
  Result.Add('+-----------+----------------+-----------------+');
  Result.Add('| ��������� | ����� �������. | ����� ��������� |');
  Result.Add('+-----------+----------------+-----------------+');
  for i := 0 to QueueList.Count - 1 do
  begin
    Result.Add('     **** ������� "' + TSMPLQueue(QueueList.Items[i])
        .QueueName + '" **** ');
    for j := 0 to TSMPLQueue(QueueList.Items[i]).Count - 1 do
      Result.Add('|' + IntToStrLen(TSMPLQueueElement
            (TSMPLQueue(QueueList.Items[i])[j]).Priority, 10)
          + ' |' + IntToStrLen(TSMPLQueueElement(TSMPLQueue(QueueList.Items[i])
              [j]).Time, 15) + ' |' + IntToStrLen
          (TSMPLQueueElement(TSMPLQueue(QueueList.Items[i])[j]).TransactID,
          16) + ' |');
    Result.Add('+-----------+----------------+-----------------+');
  end;
end;

function TSMPL.Poisson(M: integer): integer;
const
  p = 0.1;
  Accuracy = 4;

var
  i, x: integer;

begin
  x := 0;
  for i := 1 to Round(M / p) div Accuracy do
    if random < p then
      inc(x, Accuracy);
  Result := x;
end;

procedure TSMPL.ReInit;
var
  i: integer;

begin
  for i := 0 to EventList.Count - 1 do
    TSMPLEvent(EventList.Items[i]).Free;
  EventList.Clear;

  for i := 0 to DeviceList.Count - 1 do
    TSMPLDevice(DeviceList.Items[i]).Free;
  DeviceList.Clear;

  for i := 0 to QueueList.Count - 1 do
    TSMPLQueue(QueueList.Items[i]).Free;
  QueueList.Clear;

  fTime := 0;
  fTrace.Clear;
end;

function TSMPL.Report: TStringList;
var
  tmp : TStringList;

begin
  Result := TStringList.Create;

  Result.Add('����� �������������: '+IntToStr(fTime)+' ������.');
  Result.Add('');

  tmp := ReportDevices;
  Result.AddStrings(tmp);
  Result.Add('');
  tmp.Free;

  tmp := ReportQueues;
  Result.AddStrings(tmp);
  tmp.Free;
end;

function TSMPL.ReportDevices: TStringList;
var
  st : string;
  i: Integer;
  device : TSMPLDevice;

begin
  Result := TStringList.Create;
  Result.Add('                     ����������');
  Result.Add('+----------------+------------+-----------+------------+');
  Result.Add('| ��� ���������� | ��.��.���. | % ���.��. | ���. ����. |');
  Result.Add('+----------------+------------+-----------+------------+');
  for i := 0 to DeviceList.Count - 1 do
    begin
      device := TSMPLDevice(DeviceList.Items[i]);
      st := '|';

      st := st + StrToStrLen(device.DeviceName, 15) + ' |';
      if device.fCompleteUsageCount = 0 then
        st := st + StrToStrLen('------', 11) + ' |'
       else
        st := st + FloatToStrLen(device.fTimeOfUsage / device.fCompleteUsageCount, 11) + ' |';
      if fTime = 0 then
        st := st + StrToStrLen('------', 10) + ' |'
       else
        st := st + FloatToStrLen(100*(device.fTimeOfUsage / fTime), 10) + ' |';
      st := st + IntToStrLen(device.fCompleteUsageCount, 11) + ' |';
      Result.Add(st);
    end;
  Result.Add('+----------------+------------+-----------+------------+');
end;

function TSMPL.ReportQueues: TStringList;
var
  i: Integer;
  queue : TSMPLQueue;
  st : string;
  averageLength : real;

begin
  Result := TStringList.Create;
  Result.Add('                              �������');
  Result.Add('+----------------+----------+----------+---------------------------+');
	Result.Add('|  ��� �������   |           �����           |');
	Result.Add('|                |  Max  | ������� | ������� |');
    Result.Add('+----------------+-------+---------+---------+');
  for i := 0 to QueueList.Count - 1 do
    begin
      queue := TSMPLQueue(QueueList.Items[i]);
      if fTime = 0 then
        averageLength := 0
       else
        averageLength := queue.STQ / fTime;
      st := '|'+ StrToStrLen(queue.QueueName, 15) + ' |';
      {if queue.DeletedCount = 0 then
        st := st + StrToStrLen('----', 9) + ' |' + StrToStrLen('----', 9) + ' |'
       else
        st := st + FloatToStrLen(queue.SW / queue.DeletedCount, 9) + ' |' + FloatToStrLen(sqrt(abs(queue.SW2 / queue.DeletedCount - sqr(queue.SW / queue.DeletedCount))), 9) + ' |';}
      st := st + IntToStrLen(queue.Max, 6) + ' |' + FloatToStrLen(averageLength, 8) + ' |' + IntToStrLen(queue.Count, 8) + ' |';
      Result.Add(st);
    end;
  Result.Add('+----------------+----------+----------+-------+---------+---------+');
end;

function TSMPL.UniForm(minValue, maxValue: integer): integer;
begin
  Result := minValue + random(maxValue - minValue + 1);
end;

procedure TSMPL.Monitor(var sts: TStringList);
var
  tmp : TStringList;

begin
  if (sts <> Nil) then
    begin
      tmp := Monitor;
      sts.AddStrings(tmp);
      tmp.Free;
    end
   else
    sts := Monitor;
end;

procedure TSMPL.MonitorDevices(var sts: TStringList);
var
  tmp : TStringList;

begin
  if (sts <> Nil) then
    begin
      tmp := MonitorDevices;
      sts.AddStrings(tmp);
      tmp.Free;
    end
   else
    sts := MonitorDevices;
end;

procedure TSMPL.MonitorEvents(var sts: TStringList);
var
  tmp : TStringList;

begin
  if (sts <> Nil) then
    begin
      tmp := MonitorEvents;
      sts.AddStrings(tmp);
      tmp.Free;
    end
   else
    sts := MonitorEvents;
end;

procedure TSMPL.MonitorQueues(var sts: TStringList);
var
  tmp : TStringList;

begin
  if (sts <> Nil) then
    begin
      tmp := MonitorQueues;
      sts.AddStrings(tmp);
      tmp.Free;
    end
   else
    sts := MonitorQueues;
end;

procedure TSMPL.Report(var sts: TStringList);
var
  tmp : TStringList;

begin
  if (sts <> Nil) then
    begin
      tmp := Report;
      sts.AddStrings(tmp);
      tmp.Free;
    end
   else
    sts := Report;
end;

procedure TSMPL.ReportDevices(var sts: TStringList);
var
  tmp : TStringList;

begin
  if (sts <> Nil) then
    begin
      tmp := ReportDevices;
      sts.AddStrings(tmp);
      tmp.Free;
    end
   else
    sts := ReportDevices;
end;

procedure TSMPL.ReportQueues(var sts: TStringList);
var
  tmp : TStringList;

begin
  if (sts <> Nil) then
    begin
      tmp := ReportQueues;
      sts.AddStrings(tmp);
      tmp.Free;
    end
   else
    sts := ReportQueues;
end;

{ TSMPLDevice }

constructor TSMPLDevice.Create(DeviceName: String; SMPL: TSMPL);
begin
  fDeviceName := DeviceName;
  fSMPL := SMPL;
  fLastTimeReserve := 0;
  fTransactID := 0;
  fCompleteUsageCount := 0;
  fTimeOfUsage := 0;
end;

procedure TSMPLDevice.Release;
begin
  if fSMPL = nil then
  begin
    fSMPL.Error(
      '������ : �� ��������������� ����� ������������� ��� ���������� ' +
        fDeviceName);
    exit;
  end;
  fSMPL.AddTrace('[' + IntToStr(fSMPL.fTime) + '] Release ' + fDeviceName +
      ': Transact ' + IntToStr(fTransactID));

  if fTransactID = 0 then
  begin
    fSMPL.Error('������ : ������� ���������� ��������� ���������� ' +
        fDeviceName);
    exit;
  end;

  fTransactID := 0;
  fTimeOfUsage := fTimeOfUsage + (fSMPL.fTime - fLastTimeReserve);
  inc(fCompleteUsageCount);
end;

procedure TSMPLDevice.Reserve(TransactID: integer);
begin
  if fSMPL = nil then
  begin
    fSMPL.Error(
      '������ : �� ��������������� ����� ������������� ��� ���������� ' +
        fDeviceName);
    exit;
  end;
  fSMPL.AddTrace('[' + IntToStr(fSMPL.fTime) + '] Reserve ' + fDeviceName +
      ': Transact ' + IntToStr(TransactID));

  if fTransactID <> 0 then
  begin
    fSMPL.Error('������ : ������� ������ ��� ������� ���������� ' +
        fDeviceName);
    exit;
  end;

  fTransactID := TransactID;
  fLastTimeReserve := fSMPL.fTime;
end;

{ TSMPLQueue }

procedure TSMPLQueue.Add(const Value: TSMPLQueueElement);
var
  i: integer;
  index: integer;

begin
  if Value = nil then
  begin
    fSMPL.Error('������: ���������� ������� �������� � �������');
    exit;
  end;
  fSMPL.AddTrace('[' + IntToStr(fSMPL.fTime) + '] EnQueue in ' + fQueueName +
      ': Transact ' + IntToStr(Value.TransactID) + ', Priority ' + IntToStr
      (Value.Priority) + ', Stage ' + IntToStr(Value.Stage));

  index := 0;

  if fQueueList = nil then
  begin
    fSMPL.Error('������: ���������� � �� ������������������ ������� ' +
        fQueueName);
    exit;
  end;

  for i := fQueueList.Count - 1 downto 0 do
    if Value.Priority <= TSMPLQueueElement(fQueueList.Items[i]).Priority then
    begin
      index := i + 1;
      break;
    end;

  if index = fQueueList.Count then
    fQueueList.Add(Value)
  else
    fQueueList.Insert(index, Value);

  // ����������
  fSTQ := fSTQ + (fQueueList.Count - 1) * (fSMPL.fTime - fLastUpdate);
  if fQueueList.Count > fMax then
    fMax := fQueueList.Count;
  fLastUpdate := fSMPL.fTime;
end;

procedure TSMPLQueue.Add(TransactID, Priority, Stage: integer);
var
  elem: TSMPLQueueElement;

begin
  if fSMPL <> nil then
    elem := TSMPLQueueElement.Create(TransactID, fSMPL.fTime, Priority, Stage)
  else
  begin
    fSMPL.Error('������ : �� ��������������� ����� ������������� ��� ������� '
        + fQueueName);
    exit;
  end;
  Self.Add(elem);
end;

procedure TSMPLQueue.Clear;
var
  i: integer;

begin
  if fQueueList <> Nil then
  begin
    for i := fQueueList.Count - 1 downto 0 do
      if fQueueList.Items[i] <> Nil then
        TSMPLQueueElement(fQueueList.Items[i]).Free;
    fQueueList.Free;
  end;

  fQueueList := TList.Create;
  fMax := 0;
  fSTQ := 0;
  fSW := 0;
  fSW2 := 0;
  fLastUpdate := 0;
  fDeletedCount := 0;
end;

constructor TSMPLQueue.Create(QueueName: String; SMPL: TSMPL);
begin
  Clear;
  fQueueName := QueueName;
  fSMPL := SMPL;
end;

destructor TSMPLQueue.Destroy;
var
  i: integer;

begin
  if fQueueList <> Nil then
  begin
    for i := fQueueList.Count - 1 downto 0 do
      if fQueueList.Items[i] <> Nil then
        TSMPLQueueElement(fQueueList.Items[i]).Free;
    fQueueList.Free;
  end;

  inherited;
end;

function TSMPLQueue.getHead: TSMPLQueueElement; // ��������� ���������
begin
  Result := Nil;

  if fSMPL = nil then
  begin
    fSMPL.Error('������ : �� ��������������� ����� ������������� ��� ������� '
        + fQueueName);
    exit;
  end;
  Result := TSMPLQueueElement(fQueueList.Items[0]);
  if (fQueueList = nil) or (fQueueList.Count = 0) or
    (TSMPLQueueElement(fQueueList.Items[0]) = nil) then
  begin
    fSMPL.Error('������ : ���������� ������� �������� ������� �� ������ ' +
        fQueueName);
    exit;
  end;

  fSMPL.AddTrace('[' + IntToStr(fSMPL.fTime) + '] Head ' + fQueueName +
      ': Transact ' + IntToStr(Result.TransactID) + ', Priority ' + IntToStr
      (Result.Priority) + ', Stage ' + IntToStr(Result.Stage));

  // ����������
  fSTQ := fSTQ + fQueueList.Count * (fSMPL.fTime - fLastUpdate);
  fSW := fSW + (fSMPL.fTime - Result.fTime);
  fSW2 := fSW2 + sqr(fSMPL.fTime - Result.fTime);
  fLastUpdate := fSMPL.fTime;
  inc(fDeletedCount);

  // ��������
  fQueueList.Delete(0);
end;

function TSMPLQueue.getLength: integer;
begin
  Result := 0;

  if fQueueList <> Nil then
    Result := fQueueList.Count
  else
  begin
    fSMPL.Error('������ : ������� ' + fQueueName + ' �� ����������������');
    exit;
  end;
end;

function TSMPLQueue.getSMPLQueueElement(Index: integer): TSMPLQueueElement;
begin
  Result := Nil;

  if fQueueList <> Nil then
    if Index < fQueueList.Count then
      Result := TSMPLQueueElement(fQueueList.Items[Index])
    else
    begin
      fSMPL.Error('������ : ������� � ������� ' + IntToStr(Index)
          + ' � ������� ' + fQueueName + ' �����������');
      exit;
    end
    else
    begin
      fSMPL.Error('������ : ��� ������� ' + fQueueName);
      exit;
    end;
end;

{ TSMPLQueueElement }

constructor TSMPLQueueElement.Create
  (TransactID, Time, Priority, Stage: integer);
begin
  fStage := Stage;
  fPriority := Priority;
  fTransactID := TransactID;
  fTime := Time;
end;

{ TSMPLEvent }

constructor TSMPLEvent.Create(EventType, Time, TransactID: integer);
begin
  fTime := Time;
  fType := EventType;
  fTransactID := TransactID;
end;

end.
