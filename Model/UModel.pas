unit UModel;

interface

uses SysUtils, smpl, StdCtrls, Classes, System.Generics.Collections;

Const
  // �������
  EV_INCOMING_PARTION = 1; // ������ ������
  EV_FINISHED_PREDV_OBR1 = 2; // ��������� ��������������� ��������� 1
  EV_FINISHED_PREDV_OBR2 = 3; // ��������� ��������������� ��������� 2
  EV_FINISHED_ASSEMBLING = 4; // ����� ������
  EV_FINISHED_CALIBR = 5; // ����� �����������
  EV_STOP = 7; // ���������� �������������

  DEVICE_FREE_STATUS = 0;

type
    ModelTransact = class;

    ModelEvent = class (TSMPLEvent)
    public
        mTransact1: ModelTransact;
        mTransact2: ModelTransact;

        constructor Create(pEventID, pRelativeTime: integer; transact: ModelTransact) overload;
        constructor Create(pEventID, pRelativeTime: integer; transact1, transact2: ModelTransact) overload;
    end;

    ModelQueueElement = class (TSMPLQueueElement)
    public
       mTransact: ModelTransact;

       constructor Create(transact: ModelTransact);
    end;

    SimulationModel = class;

    ModelTransact = class
    public
        mModel: SimulationModel;
        TransactID : integer;
        mComingTime: Integer;
        mPredvObrType: integer;
        mTimeOfPredvObr: Integer;

        constructor Create(model: SimulationModel);
    end;

    SimulationModel = class
    public
        mDeviceT2 : TSMPLDevice; // ���������� ��������������� ��������� 1
        mDeviceT3 : TSMPLDevice; // ���������� ��������������� ��������� 2
        mDeviceT4 : TSMPLDevice; // ���������� ������
        mDeviceT5 : TSMPLDevice; // ���������� ����������

        mQueue1 : TSMPLQueue; //������� ��� ����������� ������
        mQueue2 : TSMPLQueue; //������� ��� �������, ��������� ��������������� ��������� 1
        mQueue3 : TSMPLQueue; //������� ��� �������, ��������� ��������������� ��������� 2
        mQueue4 : TSMPLQueue; //������� ��� ������� ��������� ������

        mTransactNum : Integer;

        mLogFile : TextFile;
        mProductCount:integer;
        mProfit: real;
        mSumProcessTime : Int64;

        mStatisticStartTime: integer;

        mSmpl : TSMPL;

        mVelchData : TArray<Integer>;

        // Model parameters
        PRM_T1 : integer; // ������� ����� ����������� ������ (���������������� �������������)
        PRM_T2_med : integer; // ������� ���� ��������������� ��������� 1
        PRM_T2_spread : integer; // �������� �������� ������� ��������������� ��������� 1 (����������� �������������)
        PRM_T3_med  : integer; // ������� ���� ��������������� ��������� 2
        PRM_T3_spread : integer; // �������� �������� ������� ��������������� ��������� 2 (����������� �������������)
        PRM_k : real; // ������� ����� ����� ��������������� ���������
        PRM_T4_med : integer; // ������� ����� ������
        PRM_T4_spread : integer; // �������� �������� ������� ������ (����������� �������������)
        PRM_T5_med : integer; // ������� ����� �����������
        PRM_T5_spread : integer; // �������� �������� ������� ���������� (����������� �������������)
        PRM_S1 : integer; // ������� �� ������������ ������ �������
        PRM_NORMAL_TIME : integer; // ���� ������ �������, ������� ������ ��1, ���������� � ���� ����� 40 �����, ��������� ������� ����������� �����.
        PRM_S2 : integer; // ���������� ������ ����� �� �������� (k - r) ������� r*S2 ������ ��������� �� ������ ������
        PRM_S3 : integer; // ���������� ������� ����������������� �������� ������ � ����������� �� m ����� ������� ��������������� �������� m*S3 ������ ��������� �� ���� �������
        PRM_MIN_POSSIBLY_T4_AND_T5 : integer; // ���������� ��������� ������������ �������� ������ � �����������


        procedure Log;
        constructor Create();
        procedure TryStartPredvObr();
        procedure TryStartAssembly();
        procedure TryStartCalibr();
        procedure ToQueue1(transact: ModelTransact);
        procedure ToQueue2(transact: ModelTransact);
        procedure ToQueue3(transact: ModelTransact);
        procedure ToQueue4(transact1, transact2: ModelTransact);

        procedure Run(modelingTime: integer; enableLog: boolean);
        destructor Destroy; override;
      end;

implementation

constructor ModelEvent.Create(pEventID, pRelativeTime: integer; transact: ModelTransact);
begin
    mTransact1 := transact;
    inherited Create(pEventID, pRelativeTime, transact.TransactID);
end;

constructor ModelEvent.Create(pEventID, pRelativeTime: integer; transact1, transact2: ModelTransact);
begin
    mTransact1 := transact1;
    mTransact2 := transact2;
    inherited Create(pEventID, pRelativeTime, transact1.TransactID);
end;

constructor ModelQueueElement.Create(transact: ModelTransact);
begin
    mTransact := transact;
    inherited Create(transact.TransactID, maxint, maxint, maxint);
end;

constructor ModelTransact.Create(model: SimulationModel);
begin
    mModel := model;
    inc(mModel.mTransactNum);
    TransactID := mModel.mTransactNum;
end;

{ ------------------- SimulationModel ------------------------- }

constructor SimulationModel.Create();
begin
    mTransactNum := 0;
    mStatisticStartTime:= 0;
end;

destructor SimulationModel.Destroy;
begin
  mSmpl.Free;
  mSmpl := Nil;
  SetLength(mVelchData, 0);
  inherited;
end;

procedure SimulationModel.Log();
begin
   AssignFile(mLogFile, 'trace.txt');
   ReWrite(mLogFile);
   Write(mLogFile, mSmpl.Trace.GetText());
   CloseFile(mLogFile);
   AssignFile(mLogFile, 'log.txt');
   ReWrite(mLogFile);
   Writeln(mLogFile, '������ �������: ', mTransactNum);
   Write(mLogFile, mSmpl.Report.GetText());
   //Writeln(mLogFile, '������ ���������: ', mMessageCount);
   CloseFile(mLogFile);
end;

procedure SimulationModel.Run(modelingTime: integer; enableLog: boolean);
var
  currentEventBase : TSMPLEvent;
  currentEvent: ModelEvent;
  event: ModelEvent;
  i:integer;
  velchCount:integer;
  stop: boolean;
begin
  // �������� ������
  mSmpl := TSMPL.Create;
  mSmpl.TraceOn := enableLog;

  mProductCount:= 0;
  mSumProcessTime := 0;

  velchCount:= round(modelingTime/10+0.5);
  SetLength(mVelchData, velchCount+1);
  for i:=0 to velchCount do begin
      mVelchData[i] := 0;
  end;

  mDeviceT2 := mSmpl.CreateDevice('Device T2');
  mDeviceT3 := mSmpl.CreateDevice('Device T3');
  mDeviceT4 := mSmpl.CreateDevice('Device T4');
  mDeviceT5 := mSmpl.CreateDevice('Device T5');

  mQueue1 := mSmpl.CreateQueue('Q1');
  mQueue2 := mSmpl.CreateQueue('Q2');
  mQueue3 := mSmpl.CreateQueue('Q3');
  mQueue4 := mSmpl.CreateQueue('Q4');

  mProfit := 0;

  // ��������� ������ ������ ������
  event := ModelEvent.Create(EV_INCOMING_PARTION, mSmpl.Exp(PRM_T1),
     ModelTransact.Create(self), ModelTransact.Create(self));
  mSmpl.AddEvent(event);

  //��������� ������� ���������� �������������
  event := ModelEvent.Create(EV_STOP, modelingTime, ModelTransact.Create(self));
  mSmpl.AddEvent(event);

  // �������� ���� �������������
  currentEventBase := Nil;
  currentEvent := nil;
  stop := false;
  repeat
    mSmpl.CauseEvent(currentEventBase); //�������� ��������� ������� �� ������� ������� � �������� ��� � currentEventBase
    currentEvent := currentEventBase as ModelEvent;
    case currentEvent.EventType of
         EV_INCOMING_PARTION : begin
            currentEvent.mTransact1.mComingTime := mSmpl.Time;
            currentEvent.mTransact2.mComingTime := mSmpl.Time;

            ToQueue1(currentEvent.mTransact1);
            ToQueue1(currentEvent.mTransact2);

            TryStartPredvObr();

            // ��������� ������ ��������� ������
            event := ModelEvent.Create(EV_INCOMING_PARTION, mSmpl.Exp(PRM_T1),
                ModelTransact.Create(self), ModelTransact.Create(self));
            mSmpl.AddEvent(event);
         end;
         EV_FINISHED_PREDV_OBR1 : begin
            mDeviceT2.Release();

            currentEvent.mTransact1.mTimeOfPredvObr:= mSmpl.Time;
            currentEvent.mTransact1.mPredvObrType := 1;

            // ��������� �������� ������
            // 100000 ������ 100 ��� ������� �������� (���� k ����� �������� ����� ���������)
            if (mSmpl.UniForm(0, 100000) > PRM_k*100000) then begin
                // ������ ������
                ToQueue2(currentEvent.mTransact1);
                TryStartAssembly();
            end
            else begin
                // ����, �������� ����� � ������� 1
                ToQueue1(currentEvent.mTransact1);
            end;

            TryStartPredvObr();
         end;
         EV_FINISHED_PREDV_OBR2 : begin
            mDeviceT3.Release();

            currentEvent.mTransact1.mTimeOfPredvObr:= mSmpl.Time;
            currentEvent.mTransact1.mPredvObrType := 2;

            // ��������� �������� ������
            // 10000 ������ 100 ��� ������� �������� (���� k ����� �������� ����� ���������)
            if (mSmpl.UniForm(0, 10000) > PRM_k*10000) then begin
                // ������ ������
                ToQueue3(currentEvent.mTransact1);
                TryStartAssembly();
            end
            else begin
                // ����, �������� ����� � ������� 1
                ToQueue1(currentEvent.mTransact1);
            end;

            TryStartPredvObr();
         end;
         EV_FINISHED_ASSEMBLING : begin
            mDeviceT4.Release();

            ToQueue4(currentEvent.mTransact1, currentEvent.mTransact2);

            TryStartCalibr();

            TryStartAssembly();
         end;
         EV_FINISHED_CALIBR : begin
            mDeviceT5.Release();

            if (mSmpl.Time > mStatisticStartTime) then begin
                // ��� �������� �������� ������� ���������
                mSumProcessTime := mSumProcessTime + (mSmpl.Time - currentEvent.mTransact1.mComingTime);
                mSumProcessTime := mSumProcessTime + (mSmpl.Time - currentEvent.mTransact2.mComingTime);

                // ������� �������
                if ((currentEvent.mTransact1.mPredvObrType = 1) and ((mSmpl.Time - currentEvent.mTransact1.mTimeOfPredvObr) > PRM_NORMAL_TIME)) or
                   ((currentEvent.mTransact2.mPredvObrType = 1) and ((mSmpl.Time - currentEvent.mTransact2.mTimeOfPredvObr) > PRM_NORMAL_TIME)) then
                begin
                    // ���� ������ �������, ������� ������ ��1, ���������� � ���� ����� 10 �����,
                    // ��������� ������� ����������� �����.
                    mProfit := mProfit + 0.5 * PRM_S1;
                end
                else begin
                    mProfit := mProfit + PRM_S1;
                end;
            end;

            currentEvent.mTransact1.Free();
            currentEvent.mTransact2.Free();

            inc(mVelchData[mSmpl.Time div 10]);
            TryStartCalibr();

            inc(mProductCount);

         end;
         EV_STOP : stop:= true;
    end;
    currentEvent.Free();
    currentEvent := nil;
    currentEventBase:= nil;
  until stop;

  if enableLog then begin
    Log();
  end;
end;

procedure SimulationModel.ToQueue1(transact: ModelTransact);
var queueElement : ModelQueueElement;
begin
    queueElement:= ModelQueueElement.Create(transact);
    mQueue1.Add(queueElement);
end;

procedure SimulationModel.ToQueue2(transact: ModelTransact);
var queueElement : ModelQueueElement;
begin
    queueElement:= ModelQueueElement.Create(transact);
    mQueue2.Add(queueElement);
end;

procedure SimulationModel.ToQueue3(transact: ModelTransact);
var queueElement : ModelQueueElement;
begin
    queueElement:= ModelQueueElement.Create(transact);
    mQueue3.Add(queueElement);
end;

procedure SimulationModel.ToQueue4(transact1,
  transact2: ModelTransact);
var queueElement : ModelQueueElement;
begin
    queueElement:= ModelQueueElement.Create(transact1);
    mQueue4.Add(queueElement);
    queueElement:= ModelQueueElement.Create(transact2);
    mQueue4.Add(queueElement);
end;


procedure SimulationModel.TryStartPredvObr();
var queueTop1: ModelQueueElement;
    queueTop2: ModelQueueElement;
    event: ModelEvent;
begin
    if (mDeviceT2.Status = DEVICE_FREE_STATUS) and
       (mDeviceT3.Status = DEVICE_FREE_STATUS) and
       (mQueue1.Count >= 2) then
    begin
        queueTop1 := mQueue1.Head as ModelQueueElement;
        mDeviceT2.Reserve(queueTop1.mTransact.TransactID);
        event := ModelEvent.Create(EV_FINISHED_PREDV_OBR1,
            mSmpl.UniForm(PRM_T2_med - PRM_T2_spread,PRM_T2_med + PRM_T2_spread),
            queueTop1.mTransact);
        mSmpl.AddEvent(event);

        queueTop2 := mQueue1.Head  as ModelQueueElement;
        mDeviceT3.Reserve(queueTop2.mTransact.TransactID);
        event := ModelEvent.Create(EV_FINISHED_PREDV_OBR2,
            mSmpl.UniForm(PRM_T3_med - PRM_T3_spread,PRM_T3_med + PRM_T3_spread),
            queueTop2.mTransact);
        mSmpl.AddEvent(event);
    end;
end;

procedure SimulationModel.TryStartAssembly();
var queueTop1: ModelQueueElement;
    queueTop2: ModelQueueElement;
    event: ModelEvent;
begin
    if (mDeviceT4.Status = DEVICE_FREE_STATUS) and
       (mQueue2.Count >= 1) and
       (mQueue3.Count >= 1) then
    begin
        queueTop1 := mQueue2.Head as ModelQueueElement;
        queueTop2 := mQueue3.Head as ModelQueueElement;

        mDeviceT4.Reserve(queueTop1.mTransact.TransactID);

        event := ModelEvent.Create(EV_FINISHED_ASSEMBLING,
            mSmpl.UniForm(PRM_T4_med - PRM_T4_spread,PRM_T4_med + PRM_T4_spread),
            queueTop1.mTransact, queueTop2.mTransact);
        mSmpl.AddEvent(event);
    end;
end;


procedure SimulationModel.TryStartCalibr();
var queueTop1: ModelQueueElement;
    queueTop2: ModelQueueElement;
    event: ModelEvent;
begin
    if (mDeviceT5.Status = DEVICE_FREE_STATUS) and
       (mQueue4.Count >= 2) then
    begin
        queueTop1 := mQueue4.Head as ModelQueueElement;
        queueTop2 := mQueue4.Head as ModelQueueElement;

        mDeviceT5.Reserve(queueTop1.mTransact.TransactID);

        event := ModelEvent.Create(EV_FINISHED_CALIBR,
            mSmpl.UniForm(PRM_T5_med - PRM_T5_spread,PRM_T5_med + PRM_T5_spread),
            queueTop1.mTransact, queueTop2.mTransact);
        mSmpl.AddEvent(event);

    end;
end;

end.
